export class SearchByLeads {

  public startCronJob(): void{
    /* Configura o cronJob */
    this.processLeads()
  }

  public saveLeadsByApiKey(apiKey: string): Object[] {
    /* Precisa da "apiKey" para pegar as Settings da Integration (auth) */

    /* POST para MS */

    const mockLeads = [
      {
        id: 4749149808,
        date_created: "2021-07-29T11:32:23.000+00:00",
        paid_amount: 1150,
        currency_id: "ARS",
        buyer: {
          id: 798779895,
          person: {
            birthdate: "1981-06-18",
            first_name: "Buyer",
            gender: "F",
            last_name: "Blackburn 798779895"
          },
          contact: {
            email: "sheree.blackburn+798779895@example.co.uk",
            phone: "54 1199351139"
          },
          ms_seller_promotions: true
        },
        order_items: [
          {
            item: {
              title: "Item De Testeo, Por Favor No Ofertar --kc:off"
            }
          },
          {
            item: {
              title: "Item De Testeo 2, Por Favor No Ofertar --kc:off"
            }
          }
        ],
        coupon: {
          id: null,
          amount: 0
        }
      }
    ]

    /* POST para Lead Manager */
    return mockLeads;
  }

  public processLeads(): void {
    /*Percorre todas as integrações ativadas
      Verificar se a field "unprocessedLeads" é all ou range.
    
      Se for "range", a requisição é:
      curl -X GET -H 'Authorization: Bearer $ACCESS_TOKEN' https://api.mercadolibre.com/shops/cda/customers?order_created_from=$DATE_FROM&order_created_to=$DATE_TO
      

      Se for "all", a requisição é: (de acordo com a Mari, para essa ser feita, a integração precisa já ter feito ao menos uma do tipo "range")
      curl -X GET -H 'Authorization: Bearer $ACCESS_TOKEN' https://api.mercadolibre.com/shops/cda/customers?scroll_id=$SCROLL_ID
      
      */

    //Faz a req. e obtém os leads (exemplo na API da Mari: https://drive.google.com/file/d/1OepNk4JaQaKwYAzCkaKgOhlFS8jjQJUs/view)

    //Mock Leads:

    const mockLeads = [
      {
        id: 4749149808,
        date_created: "2021-07-29T11:32:23.000+00:00",
        paid_amount: 1150,
        currency_id: "ARS",
        buyer: {
          id: 798779895,
          person: {
            birthdate: "1981-06-18",
            first_name: "Buyer",
            gender: "F",
            last_name: "Blackburn 798779895"
          },
          contact: {
            email: "sheree.blackburn+798779895@example.co.uk",
            phone: "54 1199351139"
          },
          ms_seller_promotions: true
        },
        order_items: [
          {
            item: {
              title: "Item De Testeo, Por Favor No Ofertar --kc:off"
            }
          },
          {
            item: {
              title: "Item De Testeo 2, Por Favor No Ofertar --kc:off"
            }
          }
        ],
        coupon: {
          id: null,
          amount: 0
        },
        paging: {
          total: 4,
          limit: 15,
          scroll_id: "YXBpY29yZS1vcmlnaW5hbC1vcmRlcnM=:ZHMtYXBpY29yZS1vcmlnaW5hbC1vcmRlcnMtMDM=:FGluY2x1ZGVfY29udGV4dF91dWlkDXF1ZXJ5QW5kRmV0Y2gBFEs1djliWDRCdW1sSjF0ZENGS3l0AAAAAHPJOh0WRUlRNGJVc3FTUWk5ZUtLN0NEM2NDQQ=="
        }
      }
    ]

    //POST para Lead Manager

  }

}