import { Request, Response, Router } from "express";

import IntegrationController from "./controllers/IntegrationController";
import { SearchByLeads } from "./jobs/SearchByLeads";

const routes = Router();

routes.post('/integration', IntegrationController.create)
routes.get('/integration', IntegrationController.find)
routes.put('/integration', IntegrationController.update)
routes.delete('/integration', IntegrationController.remove)

routes.get('/processLead', (request: Request, response: Response) => {
 
  const apiKey: string = String(request.query.apiKey) 
  if(!apiKey){
    response.status(400)
    return response.json({error: 'MISSING_API_KEY'})
  }

  const processedLeads = new SearchByLeads().saveLeadsByApiKey(apiKey);

  response.status(200)
  response.json(processedLeads)
})

export {
  routes
}
