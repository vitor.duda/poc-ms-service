const TAFFY = require('taffydb').taffy

const integrations_db = TAFFY([
  {
    _id: '60ac24fb-6495-4cbc-b0dd-1a3bb7e028c5',
    enabled: true,
    softRemoved: false,
    apiKey: 'y4napf',
    providerKey: 'leadManager.mercadoshop',
    createdAt: new Date().toISOString(),
    updatedAt: new Date().toISOString(),
    settings: {
      msUrl: 'https://legion-extranjera.mercadoshops.com.ar',
      unprocessedLeads: 'all', //or range
      fieldsShowInConversion: [ 'Date & Time', 'Order ID', 'Total Amount', 'Ordered Items' ],
      automatedMessages: {
        enabled: true,
        message: 'Obrigado pela sua compra ${propsect.firstName}! Te deixamos este número de contato para qualquer consulta que precisa realizar.'
      }
    },
  }
])

export {
  integrations_db
}