import { Request, Response } from 'express'
import IntegrationService from '../services/IntegrationService'

const create = (request: Request, response: Response) => {
  const body = request.body
  
  const integration: Integration = IntegrationService.create(body)

  response.status(integration ? 200 : 400)

  return response.json(integration)
}

const find = async (request: Request, response: Response) => {
  const params = request.query
  const foundIntegrations = await IntegrationService.find(params)

  return response.json(foundIntegrations)
}

const update = async (request: Request, response: Response) => {
  const apiKey = request.query.apiKey
  const body = request.body

  if(!apiKey){
    response.status(400)
    return response.json({error: 'MISSING_API_KEY'})
  }

  await IntegrationService.update(apiKey, body)

  response.status(200)
  response.json({message: 'Integration Updated.'})
}

const remove = async (request: Request, response: Response) => {
  const apiKey = request.query.apiKey
  if(!apiKey){
    response.status(400)
    return response.json({error: 'MISSING_API_KEY'})
  }
  
  await IntegrationService.remove(apiKey)

  response.status(200)
  response.json({message: 'Integration Removed.'})
}


export default {
  create,
  find,
  update,
  remove
}
