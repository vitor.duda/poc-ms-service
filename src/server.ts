import express from 'express'
import "reflect-metadata"
import { routes } from './routes'

const PORT = 4002

const app = express();

app.use(express.json())

app.use('/api/v1', routes)

app.listen(PORT, () => {
  console.log('Server running on port:', PORT)
})