import {randomString} from '../utils/generateRandomString'
import {v4 as uuid} from 'uuid'
import {integrations_db as db} from '../db'

const create = (integrationSettings: object) => {
  const integration: Integration = {
    _id: uuid(),
    enabled: true,
    softRemoved: false,
    apiKey: randomString(),
    providerKey: 'leadManager.mercadoshop',
    createdAt: new Date().toISOString(),
    updatedAt: new Date().toISOString(),
    settings: integrationSettings
  }

  //TO-DO: POST to Lead Manager

  db.insert(integration)

  return integration
}

const find = async (findSettigns: object) => {
  const matchedIntegrations: Integration[] = [];

  await db(findSettigns).each((result: any) => {
    matchedIntegrations.push(result)
  })

  return matchedIntegrations
}

const update = async (apiKey: any, updateSettings: object) => {
  updateSettings = {
    ... updateSettings,
    updatedAt: new Date().toISOString()
  }
  await db({apiKey: apiKey}).update(updateSettings)
  const updated = await db({apiKey: apiKey})
}

const remove = async (apiKey: any) => {
  //TO-DO: POST to Lead Manager
  update(apiKey, {enabled: false, softRemoved: true})
  // await db({apiKey: apiKey}).remove((result: any) => console.log(result))
}

export default {
  create,
  find,
  update,
  remove
}