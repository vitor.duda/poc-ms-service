type Integration = {
  _id: string,
  enabled: boolean,
  softRemoved: boolean,
  apiKey: string,
  providerKey: string,
  createdAt: string,
  updatedAt: string,
  settings: object
}